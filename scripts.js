$(document).ready(function(){
    var templates = {
        todoListItem: $('#tmpl-todo-list-item').html()
    };
    $('.todo-list')
        .on('click', '.j-remove', function(e){
            e.preventDefault();
            $(this).closest('.todo-list__item').remove();
        })
        .on('click', '.j-more', function(e){
            e.preventDefault();
            $('.todo-list__items').append(
                $.parseHTML(templates.todoListItem)
            );
        })
        .on('click', '.j-toggle', function(e){
            e.preventDefault();
            var $item = $(this).closest('.todo-list__item'),
                $state = $item.find('.j-state'),
                isDone = $item.hasClass('todo-list__item_done');
            if (isDone) {
                $state.val(0);
                $item.removeClass('todo-list__item_done');
            } else {
                $state.val(1);
                $item.addClass('todo-list__item_done');
            }
        });
    $('.j-menu-clear').click(function(e){
        e.preventDefault();
        $('.j-form').submit();
    });
});